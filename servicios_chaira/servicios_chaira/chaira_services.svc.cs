﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using System.Web.Script.Serialization;

namespace servicios_chaira
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "chaira_services" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione chaira_services.svc o chaira_services.svc.cs en el Explorador de soluciones e inicie la depuración.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class chaira_services : Ichaira_services
    {
        DataChairaDataContext datos_chaira = new DataChairaDataContext();
        //JavaScriptSerializer jss = new JavaScriptSerializer();

        public String holaMundo(String nombre)
        {
            return "Hola Mundo " + nombre + "!";
        }

        public string Iniciar_sesion(string login, string pwd)
        {
            
            string rs;
            
            /*select (Persona.nombre + ' ' + Persona.apellido1 + ' ' + Persona.apellido2) as nombre, usuario.usuario1, Persona.id_persona  from Usuario 
                    inner join Estudiante on usuario.usuario1= Estudiante.usuario
                    inner join Persona on Estudiante.id_persona= Persona.id_persona
                    where usuario.usuario1='el.cordoba' and usuario.contrasena='eliana'*/

            var login_usu = from usu in datos_chaira.Usuario
                            join estudian in datos_chaira.Estudiante on usu.usuario1 equals estudian.usuario
                            join person in datos_chaira.Persona on estudian.id_persona equals person.id_persona
                            where usu.usuario1 == login && usu.contrasena == pwd
                            select usu.usuario1 + String.Concat(' ' + person.nombre + ' ' + person.apellido1 + ' '+ person.apellido2) +' ' +person.id_persona;
            
            foreach (var usuario in login_usu)
            {
                Console.Write(usuario);
                return rs = "Bienvenid@: " + usuario ;
                
            }
  
            return "error";
        }

        public List<string> lista_contactos(string login)
        {
            //JavaScriptSerializer jss = new JavaScriptSerializer();
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<string>));
            List<string> lista = new List<string>();
            
            var contactos = from usu in datos_chaira.Usuario
                            join estu in datos_chaira.Estudiante on usu.usuario1 equals estu.usuario
                            join matri in datos_chaira.Matricula on estu.codigo equals matri.codigo_estudiante
                            join grup in datos_chaira.Grupo on matri.id_grupo equals grup.id_grupo
                            join asig in datos_chaira.Asignatura on grup.id_asignatura equals asig.id_asignatura
                            where usu.usuario1 != login
                            group usu by new { usu.usuario1 } into grouping
                            select grouping.FirstOrDefault();

            foreach (var usua in contactos)
            {
                Console.WriteLine(usua);
                lista.Add(usua.usuario1);
            }
            
            return lista;
        }


        public List<string> lista_grupos(string login)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<string>));
            List<string> lista = new List<string>();
            string json = "";
           /* select Asignatura.id_asignatura, Asignatura.nombre_asig from Matricula
                inner join Estudiante on Matricula.codigo_estudiante= Estudiante.codigo
                inner join Grupo on Matricula.id_grupo=Grupo.id_grupo
                inner join Usuario on Estudiante.usuario= usuario.usuario1
                inner join Asignatura on Grupo.id_asignatura= Asignatura.id_asignatura
                where usuario.usuario1='el.cordoba';*/
            MemoryStream s= new MemoryStream();

            var grupos_u = from matri in datos_chaira.Matricula
                           join estudian in datos_chaira.Estudiante on matri.codigo_estudiante equals estudian.codigo
                           join grupo in datos_chaira.Grupo on matri.id_grupo equals grupo.id_grupo
                           join usu in datos_chaira.Usuario on estudian.usuario equals usu.usuario1
                           join asig in datos_chaira.Asignatura on grupo.id_asignatura equals asig.id_asignatura
                           where usu.usuario1 == login
                           group asig by new {asig.id_asignatura} into grouping
                           select grouping.FirstOrDefault();
                              

            
           //jss.Serialize(grupos_u);

           foreach (var usua in grupos_u)
            {
                Console.WriteLine(usua);
                //lista.Add(usua.id_asignatura.ToString(), usua.nombre_asig);
                //lista.Add(usua);
                //jss.Serialize(usua.id_asignatura.ToString(), usua.nombre_asig);
                json = jss.Serialize(usua.id_asignatura);
                //json= json + jss.Serialize(usua.nombre_asig);
                lista.Add(json);
               // return lista;
            }
            //ser.WriteObject(s,grupos_u);
            //String json = Encoding.Default.GetString(s.ToArray());
           
            return lista;
        }

        

        public List<string> contactos_grupos(int codigo_grupo)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<string>));
            List<string> lista = new List<string>();

           /* select Usuario.usuario1 from Usuario
                inner join Estudiante on Estudiante.usuario=usuario.usuario1
                Inner join Matricula on Estudiante.codigo=Matricula.codigo_estudiante
                inner join Grupo on Matricula.id_grupo=Grupo.id_grupo
                inner join Asignatura on Grupo.id_asignatura=Asignatura.id_asignatura
                where Asignatura.id_asignatura=20*/

            var contacto_g = from usu in datos_chaira.Usuario
                             join estu in datos_chaira.Estudiante on usu.usuario1 equals estu.usuario
                             join matri in datos_chaira.Matricula on estu.codigo equals matri.codigo_estudiante
                             join grup in datos_chaira.Grupo on matri.id_grupo equals grup.id_grupo
                             join asig in datos_chaira.Asignatura on grup.id_asignatura equals asig.id_asignatura
                             where asig.id_asignatura == codigo_grupo
                             select usu.usuario1;

            foreach (var usua in contacto_g) 
            {
                Console.WriteLine(usua);
                lista.Add(usua);
            }
            return lista;
        }
    }
}

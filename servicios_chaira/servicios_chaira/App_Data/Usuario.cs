//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace servicios_chaira.App_Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Usuario
    {
        public Usuario()
        {
            this.Docente = new HashSet<Docente>();
            this.Estudiante = new HashSet<Estudiante>();
        }
    
        public string usuario1 { get; set; }
        public string contrasena { get; set; }
    
        public virtual ICollection<Docente> Docente { get; set; }
        public virtual ICollection<Estudiante> Estudiante { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace servicios_chaira.App_Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Asignatura
    {
        public Asignatura()
        {
            this.Grupo = new HashSet<Grupo>();
        }
    
        public int id_asignatura { get; set; }
        public string nombre_asig { get; set; }
    
        public virtual ICollection<Grupo> Grupo { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Description;
using System.Text;
using System.Web;

namespace servicios_chaira
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "Ichaira_services" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface Ichaira_services
    {
        [OperationContract]
        [WebInvoke(Method = "GET",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Wrapped,
        UriTemplate = "holaMundo/{nombre}")]
        String holaMundo(String nombre);

        [OperationContract]
        //[WebInvoke]
       [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string Iniciar_sesion(string login, string pwd);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        List<string> lista_contactos(string login);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "Lista_de_contactos/{login}")]
        List<string> lista_grupos(string login);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        List<string> contactos_grupos(int codigo_grupo);
    }
}
